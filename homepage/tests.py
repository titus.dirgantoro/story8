from django.test import TestCase,Client
from django.urls import resolve
from django.http import HttpRequest
from .views import ajax_with_django_view
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Lab1UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_ajax_with_django_view_func(self):
        found = resolve('/')
        self.assertEqual(found.func, ajax_with_django_view)

    def test_there_is_p(self):
        response = Client().get('')
        self.assertContains(response, '</form>')

    def test_there_is_button_search(self):
        response = Client().get('')
        self.assertContains(response, '<input id="button" type="submit" value="search">')

    def test_there_is_a_word_TitusWeb(self):
        response = Client().get('')
        self.assertContains(response, 'TitusWeb')

    def test_there_is_a_word_Books(self):
        response = Client().get('')
        self.assertContains(response, 'Books')

    def test_there_is_a_word_Search(self):
        response = Client().get('')
        self.assertContains(response, 'Search')

    def test_fungsi_cari_bisa_dilakukan(self):
        response_get = Client().get('/getData',{'key':'a'})
        self.assertEqual(response_get.status_code,200)

class Story8TitusFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8TitusFunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(Story8TitusFunctionalTest, self).tearDown()

    def test_change_theme_and_click_accordion(self):
        selenium = self.selenium

        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')

        # find the element
        searchbox = selenium.find_element_by_id("search")
        searchbox.send_keys("Man")

        time.sleep(2)
        
        btnSearch = selenium.find_element_by_id("button")
        btnSearch.click()

        time.sleep(2)

        self.assertIn('The Descent of man', selenium.page_source)
