import requests
from django.http import JsonResponse
from django.shortcuts import render

def ajax_with_django_view(request):
    return render(request, 'ajax_django_view.html')


def getData(request):
    key = request.GET['key']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + key
    response = requests.get(url)
    response_json = response.json()

    return JsonResponse(response_json)
